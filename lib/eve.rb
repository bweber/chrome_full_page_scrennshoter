module Eve
  class Node
  end
end

Eve::Node.autoload(:Server, ROOT + '/lib/server.rb')

# Workers
Dir[ROOT + '/app/workers/*'].each do |file|
  Eve::Node.autoload File.basename(file, '.rb').classify.to_sym, file
end

#Helpers
Dir[ROOT + '/app/helpers/*'].each do |file|
  Eve::Node.autoload File.basename(file, '.rb').classify.to_sym, file
end

