class Eve::Node::Server < Sinatra::Base

  get '/' do
    raise NotImplementedError
  end

  post '/v1/screenshots' do
    worker = Eve::Node::Kwejk.new(params[:id], params[:login], params[:password], params[:dir])
    worker.run
  end

end

