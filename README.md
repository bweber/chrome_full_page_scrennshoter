Full Page Screenshot on Chrome Browser
===
Application to take full page screenshot using Selenium Worker

Used ruby: 2.2.3

usage:
---

Copy _chromedriver_ from /lib to /usr/bin

```bash
  ruby app.rb URL
```

URL - paste url for page to get full page screenshot
