class Eve::Node::ChromeWorker < Eve::Node::SeleniumWorker

  def initialize(id, login, password, result_dir)
    super
    self.driver_type = :chrome
  end


  def body_height
    @driver.execute_script("return document.getElementsByTagName('body')[0].scrollHeight;")
  end


  def screen_height
    @driver.execute_script("return window.innerHeight;")
  end


  def scroll(x, y)
    @driver.execute_script("window.scrollTo(#{x}, #{y})")
  end


  def hide_scrollbar
    @driver.execute_script("document.body.style.overflow='hidden';")
  end


  def scroll_and_shot(file_name)
    scroll(0, 0)
    b_height = body_height
    s_height = screen_height
    scrolls = calculate_scrolls b_height, s_height
    height = s_height

    hide_scrollbar

    screenshot('tmp-0')
    images_list = ["#{result_dir}/tmp-0.png"]

    (1..scrolls).each do |i|
      scroll(0, height)
      sleep 0.5
      screenshot("tmp-#{i}")
      images_list << "#{result_dir}/tmp-#{i}.png"
      height += s_height
    end

    pixels_to_crop = left_pixels(b_height, s_height, scrolls)

    crop_image(images_list.last, pixels_to_crop)
    join_images(images_list, file_name)
    remove_tmp_images
  end


  # Calculate how many scrolls we need
  def calculate_scrolls(body_height, screen_height)
    (body_height/screen_height).to_i
  end


  # Calculate left pixels to crop last image
  def left_pixels(body_height, screen_height, scrolls)
    (screen_height - (body_height - (screen_height * scrolls))).to_i
  end


  # Crop pixels from image top
  def crop_image(image, pixels)
    croped_image = Magick::ImageList.new(image)
    croped_image.crop!(0, pixels, 0, 0)
    croped_image.write(image)
  end


  # Join all images in to single image
  def join_images(images_list, out_filename)
    time = Time.now
    images = Magick::ImageList.new(*images_list)
    images.append(true).write("#{result_dir}/#{out_filename}_#{time.strftime("%d%m%Y%H%M%S")}.png")
  end


  # Remove all tmp images used to join images
  def remove_tmp_images
    Dir.chdir result_dir do |dir|
      FileUtils.rm Dir.glob('tmp-*')
    end
  end

end

