class Eve::Node::SeleniumWorker

  ISPNotRespond = Class.new(StandardError)

  attr_accessor :id, :login, :password, :result_dir, :driver_type

  def initialize(id, login, password, result_dir)
    self.id = id.to_i
    self.login = login.to_s.strip
    self.password = password.to_s.strip
    self.result_dir = ROOT + result_dir.to_s.strip
  end


  def run
    start_selenium
    begin
      process
      close_browser
    rescue => e
      close_browser
      raise ISPNotRespond, e.message
    end
  end


  def process
    raise NotImplementedError
  end


  def start_selenium
    if driver_type == :firefox
      raise NotImplementedError
    elsif driver_type == :chrome
      Selenium::WebDriver::Chrome.driver_path = CHROME_DRIVER
    end
    @driver = Selenium::WebDriver.for driver_type
  end


  def maximalize_window
    @driver.manage.window.maximize
  end


  def screenshot(file_name)
    @driver.save_screenshot("#{result_dir}/#{file_name}.png")
  end


  def close_browser
    @driver.close
  end


  def navigate(url)
    @driver.navigate.to url
  end


  def prepare_window_before_screenshot
    raise NotImplementedError
  end

end

