require_relative 'config/init'

ENV['RACK_ENV'] ||= 'development'

run Eve::Node::Server
