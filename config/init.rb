require 'rubygems'
require 'selenium-webdriver'
require 'rmagick'
require 'sinatra'
require 'minitest'
require 'minitest/autorun'
require 'mocha/mini_test'
require 'active_support/all'
require 'rest-client'

CHROME_DRIVER = File.join(File.absolute_path('./lib'), 'chromedriver')

ROOT = File.expand_path('../..', __FILE__)
RESULTS_DIR = File.join('..', 'tmp', 'results')

$: << File.join(ROOT, 'lib')
$: << File.join(ROOT, 'app')

require 'eve'
